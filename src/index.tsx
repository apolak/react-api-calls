import * as React from 'react';
import * as ReactDom from 'react-dom';
import { AppComponent } from './components/app/app.component';
import { createStore, applyMiddleware } from 'redux';
import { Provider} from 'react-redux';
import { reducer } from './reducer/index';
import { dataLoaderService } from './service/data-loader.service';

const store: any = createStore(
	reducer,
	{items:[], isLoading: false},
	applyMiddleware(dataLoaderService)
);

ReactDom.render(
	<Provider store={store}>
		<AppComponent/>
	</Provider>,
	document.getElementById('example')
);
