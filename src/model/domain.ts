export type Item = {
	name: string
}

export type State = {
	items: Item[],
	isLoading: boolean
}
