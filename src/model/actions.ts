import { Item } from './domain';
export const FETCH_ITEMS: string = 'FETCH_ITEMS';
export const FETCH_ITEMS_SUCCESS: string = 'FETCH_ITEMS_SUCCESS';
export const FETCH_ITEMS_ERROR: string = 'FETCH_ITEMS_ERROR';


export const createFetchItemsAction: Function = (): Object => {
	return {
		type: FETCH_ITEMS
	}
};

export const createFetchItemsSuccessAction: Function = (items: Item[]): Object => {
	return {
		type: FETCH_ITEMS_SUCCESS,
		items: items
	}
};

export const createFetchItemsErrorAction: Function = (err: any): Object => {
	return {
		type: FETCH_ITEMS_ERROR,
		error: err
	}
};
