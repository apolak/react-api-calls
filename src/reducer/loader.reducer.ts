import { Reducer } from 'redux';
import * as React from 'react';
import { FETCH_ITEMS, FETCH_ITEMS_ERROR, FETCH_ITEMS_SUCCESS } from '../model/actions';
import update = require('immutability-helper');

export const loaderReducer: Reducer<any> =  (state:any = false, action: any): any => {
	switch (action.type) {
		case FETCH_ITEMS:
			return true;
		case FETCH_ITEMS_ERROR:
			console.log(action.error);
			return false;
		case FETCH_ITEMS_SUCCESS:
			return false;
		default:
			return state;
	}
};
