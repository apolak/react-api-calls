import { Reducer, combineReducers } from 'redux';
import { loaderReducer } from './loader.reducer';
import { itemsReducer } from './itemsReducer.reducer';

export const reducer: Reducer<any> = combineReducers({
	items: itemsReducer,
	isLoading: loaderReducer
});
