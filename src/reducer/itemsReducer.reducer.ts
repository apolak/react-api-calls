import { Reducer } from 'redux';
import * as React from 'react';
import { FETCH_ITEMS_SUCCESS } from '../model/actions';

import update = require('immutability-helper');

export const itemsReducer: Reducer<any> =  (state:any = [], action: any): any => {
	switch (action.type) {
		case FETCH_ITEMS_SUCCESS:
			return action.items;
		default:
			return state;
	}
};
