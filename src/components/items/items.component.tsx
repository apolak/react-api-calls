import * as React from 'react';
import { Item } from '../../model/domain';

type ItemsComponentProps = {
	items: Item[],
	isLoading: boolean,
	loadData: Function
}

export class ItemsComponent extends React.Component<ItemsComponentProps, void> {
	componentWillMount() {
		this.props.loadData();
	}

	render(): JSX.Element {
		if (this.props.isLoading) {
			return <p>Loading</p>
		}

		return (
			<div>
				{
					this.props.items.map((item: Item, index: number) => {
						return <p key={index}>{item.name}</p>
					})
				}
			</div>
		)
	}
}
