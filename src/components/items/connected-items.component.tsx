import { connect } from 'react-redux';
import { State } from '../../model/domain';
import { createFetchItemsAction } from '../../model/actions';
import { ItemsComponent } from './items.component';

const mapStateToProps: any = (state: State): Object => {
	return {
		items: state.items,
		isLoading: state.isLoading
	}
};

const mapDispatchToProps: any = (dispatch: any): Object => {
	return {
		loadData: () => {
			dispatch(createFetchItemsAction())
		}
	}
};

export const ConnectedItemsComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(ItemsComponent);
