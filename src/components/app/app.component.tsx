import * as React from 'react';
import { ConnectedItemsComponent } from '../items/connected-items.component';

export class AppComponent extends React.Component<void, void> {
	render(): JSX.Element {
		return (
			<div>
				<ConnectedItemsComponent />
			</div>
		)
	}
}

