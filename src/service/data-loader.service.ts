import * as request from 'superagent';
import { Middleware } from 'redux';
import { FETCH_ITEMS, createFetchItemsErrorAction, createFetchItemsSuccessAction } from '../model/actions';

export const dataLoaderService: Middleware = ({getState}: any) => {
	return (next: any) => (action: any) => {
		next(action);

		switch(action.type) {
			case FETCH_ITEMS:
				console.log("Request sent");
				request
					.get('http://slowwly.robertomurray.co.uk/delay/7000/url/http://www.mocky.io/v2/580917c7100000700a4c6367')
					.end((err:any, res:any) => {
						if(err) {
							return next(createFetchItemsErrorAction(err));
						}
						const data = JSON.parse(res.text);

						return next(createFetchItemsSuccessAction(data.items))
					});
				break;
			default:
				break;
		}
	}
};
